﻿using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;

namespace WiredBrainCoffeeSurveys.Reports
{
    class Program
    {
        static void Main(string[] args)
        {
            bool quitApp = false;
           
            do
            {
                Console.WriteLine("Run Report (Y), Quit(N).");
                var runReport = Console.ReadLine();
                Console.WriteLine("Please specify which quarter of data: (Q1, Q2): ");
                var selectedData = Console.ReadLine();

                var surveyResults = JsonConvert.DeserializeObject<SurveyResult>(File.ReadAllText($"Data/{selectedData}.json"));
                switch (runReport)
                {
                    case "Y":
                        GenerateTasksReport(surveyResults);
                        break;
                    case "N":
                        quitApp = true;
                        break;
                    default:
                        Console.WriteLine("Invalid option");
                        break;
                }
                Console.WriteLine();
            }
            while (!quitApp);
            

        }

        private static void GenerateTasksReport(SurveyResult results)
        {
            
            //these are all single line statement

                var tasks = new List<string>();

                double responseRate = results.NumberResponded / results.NumberSurveyed;
                double unansweredCount = results.NumberSurveyed - results.NumberResponded; // expression statements

                double overallScore = (results.ServiceScore + results.CoffeeScore + results.FoodScore + results.PriceScore) / 4;

                Console.WriteLine($"Response percentage: {responseRate}");
                Console.WriteLine($"Unanswered surveys: {unansweredCount}");
                Console.WriteLine($"Overall score: {overallScore}");

                //is coffee score higher than food score?
                //would customer recommend us
                //which is popular.. granola or cappucino


                //logical comparisons
                bool higherCoffeeScore = results.CoffeeScore > results.FoodScore;
                bool customerRecommend = results.WouldRecommend >= 7;
                bool noGranolaYesCappucino = results.LeastFavoriteProduct == "Granola" && results.FavoriteProduct == "Cappucino";

                Console.WriteLine($"Is coffee score higher than food score?: {higherCoffeeScore}");
                Console.WriteLine($"Would we be recommended by customers? : {customerRecommend}");
                Console.WriteLine($"Hate granola.. Love cappucino: {noGranolaYesCappucino}");

 /************************************************************************************************************************************/

            bool isCoffeeScoreLower = results.CoffeeScore < results.FoodScore;
                if (isCoffeeScoreLower)
                {
                    tasks.Add("Investigate coffee recipes and ingredients.");
                }

            /************************************************************************************************************************************/
            //ternery
            //var newTask = overallScore > 8.0 ? "Work with leadership to reward staff." : "Work with employees for improved ideas!";
            //tasks.Add(newTask);
            tasks.Add(overallScore > 8.0 ? "Work with leadership to reward staff." : "Work with employees for improved ideas!");

                //if..else
                //if (overallScore > 8.0)
                //{
                //    tasks.Add("Work with leadership to reward staff.");
                //}
                //else
                //{
                //    tasks.Add("Work with employees for improved ideas!");
                //}

 /************************************************************************************************************************************/

            if (responseRate < .33)
                {
                    tasks.Add("Research options to improve response rate.");
                }
                else if (responseRate > .33 && responseRate < .66)
                {
                    tasks.Add("Reward participants with free coffee coupon.");
                }
                else
                {
                    tasks.Add("Reward partipants with discount coffee coupons.");
                }

            //tasks.Add(responseRate switch
            //{
            //    var rate when rate < .33 => "Research options to improve response rate.",
            //    var rate when rate < .33 && rate < .66 => "Reward participants with free coffee coupon.",
            //    var rate when rate > .66 => "Reward participants with free coffee coupon."
            //});

/*********************************************************************************************************************************/

                //lets streamline this switch..
                //switch (results.AreaToimprove)
                //{
                //    case "RewardsProgram":
                //        tasks.Add("Revisit the rewards deals.");
                //        break;
                //    case "MobileApp":
                //        tasks.Add("Contact consulting team about the app.");
                //        break;
                //    default:
                //        tasks.Add("Investigate individual comments for ideas.");
                //        break;
                //}

            //var improveTask = results.AreaToimprove switch
            tasks.Add(results.AreaToimprove switch
            {
                "RewardsProgram" => "Revisit the rewards deals.",
                "MobileApp" => "Contact consulting team about the app.",
                _ => "Investigate individual comments for ideas."
            });

            //tasks.Add(improveTask);

                File.WriteAllLines("TasksReports.csv", tasks);
        }
    }
}
