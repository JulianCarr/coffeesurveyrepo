﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WiredBrainCoffeeSurveys.Reports
{
    public  class SurveyResult
    {
        // Aggregate ratings
        public double ServiceScore { get; set; } 

        public double CoffeeScore { get; set; } 

        public double PriceScore { get; set; } 

        public double FoodScore { get; set; }

        public double WouldRecommend { get; set; } = 6.5;
        public string AreaToimprove { get; set; } = "MobileApp";
        public string FavoriteProduct { get; set; } = "Cappucino";

        public string LeastFavoriteProduct { get; set; } = "Granola";

        // Aggregate counts
        public double NumberSurveyed { get; set; } = 500;

        public double NumberResponded { get; set; } = 325;

        public double NumberRewardsMembers { get; set; } = 130;
    }
}
